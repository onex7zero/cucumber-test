package steps;

import com.wayne.Hello;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertEquals;
/**
 * Created by wayne on 2017/3/19.
 */
public class HelloStepdefs {

    Hello hello = null;
    String hi = null;

    @Given("^I have a greeting application with \"([^\"]*)\"$")
    public void iHaveAGreetingApplicationWith(String arg0) throws Throwable {
        hello = new Hello(arg0);
    }

    @When("^I ask it to say hi$")
    public void iAskItToSayHi() throws Throwable {
        hi =hello.sayHi();
    }

    @Then("^I receive \"([^\"]*)\"$")
    public void iReceive(String arg0) throws Throwable {
        assertEquals(arg0,hi);
    }
}
