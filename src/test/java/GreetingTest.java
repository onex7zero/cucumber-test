/**
 * Created by wayne on 2017/3/19.
 */
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/Features/hello_world.feature",
        glue = {"steps"})
public class GreetingTest {
}
