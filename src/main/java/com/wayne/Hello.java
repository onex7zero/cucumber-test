package com.wayne;

/**
 * Created by wayne on 2017/3/19.
 */
public class Hello {
    private final String greeting;

    public Hello(String greeting)
    {
        this.greeting = greeting;
    }
    public String sayHi()
    {
        return greeting + " World";
    }
}
